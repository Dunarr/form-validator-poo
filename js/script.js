const sendElement = document.querySelector('input[type="submit"]')
const messageContainer = document.getElementById("message-container")

function notEmpty(value){
    return value.length > 0
}

function minLength(value){
    return value.length >= 8
}

const rules = [
    {
        id: "username",
        validator: minLength,
        message: "Identifiant trop court"
    },
    {
        id: "username",
        validator: notEmpty,
        message: "Identifiant obligatoire"
    },
    {
        id: "password",
        validator: minLength,
        message: "Mot de passe trop court trop court"
    },
    {
        id: "password",
        validator: function (value){
            return !!value.match(/[$^%@]/)
        },
        message: "Le mot de passe doit contenir au moin un caractère spécial"
    },
    {
        id: "password",
        validator: function (value){
            return !!value.match(/[0-9]/)
        },
        message: "Le mot de passe doit contenir au moins un chiffre"
    },
    {
        id: "confirm-password",
        validator: function (value){
            return value === document.getElementById("password").value
        },
        message: "Les mots de passe ne correspondent pas"
    },
    {
        id: "mail",
        validator: function (value){
            const atPos = value.indexOf('@')
            const dotPos = value.lastIndexOf('.')
            if(atPos === -1 || dotPos === -1){
                return false
            }
            return atPos < dotPos
        },
        message: "Le mail est invalide"
    },
    {
        id: "tel",
        validator: function (value){
            return value.match(/^[0-9]{10}$/)
        },
        message: "Le téléphone est invalide"
    },

]


sendElement.addEventListener("click", function (){
    error = document.createElement("div")
    error.classList.add("error")
    success = document.createElement("div")
    success.classList.add("success")
    success.innerHTML = "compte créé"
    var hasErrors = false
    for (rule of rules){
        const inputValue = document.getElementById(rule.id).value
        if(!rule.validator(inputValue)){
            hasErrors = true;
            error.innerHTML+=rule.message+"\n"
        }
    }
    messageContainer.innerHTML = ""
    messageContainer.appendChild(hasErrors?error:success)
})
